import * as React from 'react'
import { shallow } from 'enzyme'
import { DebounceInput } from '../components/Modal/DebounceInput'

describe('<DebounceInput />', () => {
	it('Handle input correctly', () => {
		const mockFn = jest.fn()
		const wrapper = shallow(
			<DebounceInput
				highlight={false}
				placeholder="Mock Input"
				handleOnChange={mockFn}
			/>
		)

		wrapper.find('input').simulate('change', {
			target: { value: 'My new value' },
			persist: () => {},
		})

		// expect(mockFn).toHaveBeenCalledTimes(1)
	})

	it('Highlight render correctly', () => {
		const mockFn = jest.fn()
		const wrapper = shallow(
			<DebounceInput
				highlight={false}
				placeholder="Mock Input"
				handleOnChange={mockFn}
			/>
		)

		wrapper.setProps({ highlight: true })
		expect(wrapper.find('.fields').hasClass('highlight')).toEqual(true)
	})
})
