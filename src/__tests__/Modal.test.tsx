import * as React from 'react'
import { shallow } from 'enzyme'
import { Modal } from '../components/Modal/index'

describe('<Modal />', () => {
	it('Hide correctly', () => {
		const mockFn = jest.fn()
		const wrapper = shallow(<Modal handleHideModal={mockFn} />)

		wrapper.find('.mask').simulate('click')
		expect(mockFn).toHaveBeenCalledTimes(1)
	})

	it('Submit correct', () => {
		const mockFn = jest.fn()
		const wrapper = shallow(<Modal handleHideModal={mockFn} />)

		// correct
		wrapper.setState({
			fullName: 'Dante',
			email: 'dante@gmail.com',
			confirmEmail: 'dante@gmail.com',
			done: true,
		})

		expect(wrapper.find('.modal').hasClass('done')).toEqual(true)

		// can be hiden
		wrapper.find('.submit').simulate('click')
		expect(mockFn).toHaveBeenCalledTimes(1)
	})

	it('Submit erros', () => {
		const mockFn = jest.fn()
		const wrapper = shallow(<Modal handleHideModal={mockFn} />)

		// submit without any input
		wrapper.find('.submit').simulate('click')
		expect(wrapper.state('errors')).toEqual(['name', 'format'])

		// name is too short
		wrapper.setState({
			fullName: 'ab',
			email: 'abc@gmail.com',
			confirmEmail: 'abc@gmail.com',
		})
		wrapper.find('.submit').simulate('click')
		expect(wrapper.state('errors')).toEqual(['name'])

		// email is not same
		wrapper.setState({
			fullName: 'Dante',
			email: 'abc@gmail.com',
			confirmEmail: 'ab@gmail.com',
		})
		wrapper.find('.submit').simulate('click')
		expect(wrapper.state('errors')).toEqual(['inconsistent'])

		// email is invalidate
		wrapper.setState({
			fullName: 'Dante',
			email: 'abcgmail.com',
			confirmEmail: 'ab@gmail.com',
		})
		wrapper.find('.submit').simulate('click')
		expect(wrapper.state('errors')).toEqual(['format', 'inconsistent'])
	})
})
