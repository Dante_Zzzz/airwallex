import * as React from 'react'
import { shallow } from 'enzyme'
import { ErrorMsg } from '../components/Modal/ErrorMsg'

describe('<ErrorMsg />', () => {
	it('Renders correctly', () => {
		const initErrors = []
		const initServerError = false

		const wrapper = shallow(
			<ErrorMsg errors={initErrors} serverError={initServerError} />
		)
		expect(wrapper.find('.error')).toHaveLength(0)

		wrapper.setProps({ serverError: true })
		expect(wrapper.find('.error')).toHaveLength(1)

		wrapper.setProps({ errors: ['inconsistent', 'name'] })
		expect(wrapper.find('.error')).toHaveLength(3)
	})
})
