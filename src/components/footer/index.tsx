import * as React from 'react'
import './footer.scss'

export class Footer extends React.Component<{}> {
	render() {
		return (
			<div className="footer">
				<p>Made with love in Melbourne.</p>
				<p>© 2016 Broccoli & Co. All rights reserved</p>
			</div>
		)
	}
}
