import * as React from 'react'
import 'normalize.css'
import './app.scss'

import { Header } from './Header'
import { Main } from './Main'
import { Modal } from './Modal'
import { Footer } from './Footer'

export class App extends React.Component<{}> {
	state = {
		modalVisible: false,
	}

	showModal = () => {
		this.setState({ modalVisible: true })
	}

	hideModal = () => {
		this.setState({ modalVisible: false })
	}

	render() {
		const { modalVisible } = this.state

		return (
			<div className="wrapper">
				<Header />
				<Main handleShowModal={this.showModal} />
				<Footer />
				{modalVisible && <Modal handleHideModal={this.hideModal} />}
			</div>
		)
	}
}
