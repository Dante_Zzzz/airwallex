export interface ModalProps {
	handleHideModal: (event: React.MouseEvent<any>) => void
}

export interface ModalState {
	fullName: string
	email: string
	confirmEmail: string
	errors: string[]
	errorRes: boolean
	submitting: boolean
	done: boolean
}

export interface DebounceInputProps {
	placeholder: string
	highlight: boolean
	handleOnChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export interface ErrorMsgProps {
	errors: string[]
	serverError: boolean
}
