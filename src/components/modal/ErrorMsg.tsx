import * as React from 'react'
import { ErrorMsgProps } from './modal.d'

const errorMapping = {
	inconsistent: 'The email address are inconsistent',
	format: 'E-mail format error',
	server: 'Error Message from server here',
	name: 'Name needs to be at least 3 characters',
}

export const ErrorMsg = (props: ErrorMsgProps) => {
	const { errors, serverError } = props

	return (
		<>
			{errors.map(e => (
				<p className="error" key={e}>
					{errorMapping[e]}
				</p>
			))}
			{serverError && <p className="error">{errorMapping.server}</p>}
		</>
	)
}
