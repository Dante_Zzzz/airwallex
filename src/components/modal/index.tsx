import * as React from 'react'
import axios, { AxiosInstance } from 'axios'
import * as Isemail from 'isemail'
import { ModalProps, ModalState } from './modal.d'
import { DebounceInput } from './DebounceInput'
import { ErrorMsg } from './ErrorMsg'

import './modal.scss'

const http = axios.create()

export class Modal extends React.Component<ModalProps, ModalState> {
	state = {
		fullName: '',
		email: '',
		confirmEmail: '',
		errors: [],
		errorRes: false,
		submitting: false,
		done: false,
	}

	validator = {
		name: () => this.state.fullName.length >= 3,
		format: () =>
			Isemail.validate(this.state.email) &&
			Isemail.validate(this.state.confirmEmail),
		inconsistent: () => this.state.confirmEmail === this.state.email,
	}

	handleNameChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		this.setState({ fullName: e.target.value })
	}

	handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		this.setState({ email: e.target.value })
	}

	handleConfirmEmailChange = (
		e: React.ChangeEvent<HTMLInputElement>
	): void => {
		this.setState({ confirmEmail: e.target.value })
	}

	validateAndHighlightErrors = () => {
		const errors: string[] = []

		Object.keys(this.validator).forEach(rule => {
			if (!this.validator[rule]()) errors.push(rule)
		}, this)

		this.setState({ errors })

		return errors
	}

	handleSubmit = () => {
		const errors = this.validateAndHighlightErrors()
		if (errors.length) return

		this.setState({ submitting: true })
		const payload = {
			name: this.state.fullName,
			email: this.state.email,
		}

		http.post(
			'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth',
			payload
		)
			.then(res => {
				this.setState({
					errorRes: false,
					submitting: false,
					done: true,
				})
			})
			.catch(e => {
				this.setState({ errorRes: true, submitting: false })
			})
	}

	render() {
		const { errors, errorRes, submitting, done } = this.state

		return (
			<>
				<div className="mask" onClick={this.props.handleHideModal} />
				{done ? (
					<div className="modal done">
						<p className="modal-title">—— All done ——</p>
						<p className="modal-body">
							You will be one of the first to experience Broccoli
							& Co. when we launch.
						</p>
						<input
							className="submit"
							type="button"
							value="ok"
							onClick={this.props.handleHideModal}
						/>
					</div>
				) : (
					<div className="modal">
						<p className="modal-title">—— Request An Invite ——</p>
						<DebounceInput
							highlight={errors.includes('name')}
							placeholder="Full name"
							handleOnChange={this.handleNameChange}
						/>
						<DebounceInput
							highlight={
								errors.includes('format') ||
								errors.includes('inconsistent')
							}
							placeholder="Email"
							handleOnChange={this.handleEmailChange}
						/>
						<DebounceInput
							highlight={
								errors.includes('format') ||
								errors.includes('inconsistent')
							}
							placeholder="Confirm Email"
							handleOnChange={this.handleConfirmEmailChange}
						/>
						<input
							className={
								submitting ? 'submit disabled' : 'submit'
							}
							type="button"
							value={
								submitting ? 'Sending, please wait....' : 'Send'
							}
							disabled={submitting}
							onClick={this.handleSubmit}
						/>
						<ErrorMsg errors={errors} serverError={errorRes} />
					</div>
				)}
			</>
		)
	}
}
