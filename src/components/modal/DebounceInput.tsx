import * as React from 'react'
import { DebounceInputProps } from './modal.d'
const debounce = require('lodash/debounce')

export const DebounceInput = (props: DebounceInputProps) => {
	const { placeholder, handleOnChange, highlight } = props

	const debounceEventHandler = (...args) => {
		const debounced = debounce(...args)
		return e => {
			e.persist()
			return debounced(e)
		}
	}

	return (
		<input
			className={highlight ? 'highlight fields' : 'fields'}
			type="text"
			placeholder={placeholder}
			onChange={debounceEventHandler(handleOnChange, 200)}
		/>
	)
}
