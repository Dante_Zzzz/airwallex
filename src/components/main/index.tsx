import * as React from 'react'
import './main.scss'

interface MainProps {
	handleShowModal: (event: React.MouseEvent<HTMLInputElement>) => void
}

export class Main extends React.Component<MainProps, {}> {
	render() {
		return (
			<div className="main">
				<h1>A Better Way</h1>
				<h1>to enjoy every day.</h1>
				<p>Be the first to know when we launch.</p>
				<input
					className="invite"
					type="button"
					value="Request an invite"
					onClick={this.props.handleShowModal}
				/>
			</div>
		)
	}
}
