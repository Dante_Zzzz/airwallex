import * as React from 'react'
import './header.scss'

export class Header extends React.Component<{}> {
	render() {
		return (
			<div className="header">
				<h3>BROCCOLI & CO.</h3>
			</div>
		)
	}
}
