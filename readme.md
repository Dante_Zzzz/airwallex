## Airwallex frontend code challenge

### How to start

1. Run `yarn install` or `npm install`
2. Run `yarn start:dev` or `npm run start:dev`
3. The page will be opened automatically after build. Or you can visit `http://localhost:8080/`.

### How to run test

You can just run `yarn test`.
